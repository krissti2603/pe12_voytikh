class Product extends Base {
    constructor({title, price, description = ''}) {
        super();

        if(!title) {
            throw new Error("Missed mandatory field!")
        }
        this.title = title;
        this.price = price;
        this.description = description;

    }

}