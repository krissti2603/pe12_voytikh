document.addEventListener( 'DomContentLoaded', onReady);


let o = {
    title: 'hello'
};

function onReady() {

   // let o = {};
    Object.defineProperty(o, 'property1', {
        writable: false,
        configurable: false,
        enumerable: false,

        value: function() {
            return Math.random() * 100;
        }
    });
};