$(document).ready(function () {
    //CHANGING OF THE SURVICE BLOCK CONTENT ON 'CLICK'//
    const serviceText = {
        web_design: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptates, facilis tempora ad architecto, obcaecati maiores iste non laudantium perspiciatis cupiditate impedit dolorem fugiat numquam aliquam. Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis. Excepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error repellat repellendus ratione, et cupiditate quia!",
        graphic_design: "Illum voluptates, facilis tempora ad architecto, obcaecati maiores iste non laudantium perspiciatis cupiditate impedit dolorem fugiat numquam aliquam. Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis. Excepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error",
        online_support: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptates, facilis tempora ad architecto, obcaecati maiores iste non laudantium perspiciatis cupiditate impedit dolorem fugiat numquam aliquam. Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis. Excepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error repellat repellendus ratione, et cupiditate quia!",
        app_design: "Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis. Excepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error repellat repellendus ratione, et cupiditate quia!",
        online_marketing: "Dignissimos consequuntur, aperiam! Quaerat nesciunt porro amet ipsa adipisci eius molestiae itaque, quas, sit ab vitae qui quisquam deleniti vel architecto sunt delectus libero veniam expedita beatae ad placeat distinctio voluptates perferendis.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptates, facilis tempora ad architecto, obcaecati maiores iste non laudantium perspiciatis cupiditate impedit dolorem fugiat numquam aliquam.",
        seo_service: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum voluptatesExcepturi beatae atque tempore nostrum veritatis, sit similique fugit maxime possimus sapiente ullam repellat tempora doloribus consequuntur veniam. Veritatis ullam odit, repudiandae earum suscipit error repellat repellendus ratione, et cupiditate quia!"
    };
    $(".service_theme").click((event) => {
        $(".service_theme").removeClass("service_active");
        $(event.target).addClass("service_active");
        let themeKey = $('.service_active').attr('data-theme');
        let textKey = themeKey.split("-").join("_");
        $("#service_img").attr("src", `images/service-${themeKey}.jpg`);
        switch (true) {
            case themeKey == "web-design":
                $(".service_text>p").html(serviceText.web_design);
                break;
            case themeKey == "graphic-design":
                $(".service_text>p").html(serviceText.graphic_design);
                break;
            case themeKey == "online-support":
                $(".service_text>p").html(serviceText.online_support);
                break;
            case themeKey == "app-design":
                $(".service_text>p").html(serviceText.app_design);
                break;
            case themeKey == "online-marketing":
                $(".service_text>p").html(serviceText.online_marketing);
                break;
            case themeKey == "seo-service":
                $(".service_text>p").html(serviceText.seo_service);
                break;
        }
    });
    let btnClicked = 0;

    $('.wrapper-work > .button .load-more').click(function () {
        const contentDiv = $('.wrapper-work .tabs-content > div:last-child');
        const animationDiv = $('.wrapper-work .container');

        btnClicked += 1;
        animationDiv.slideDown("slow").css('display', 'flex');

        setTimeout(function () {
            animationDiv.slideDown("slow").css('display', 'none');
            dataFromServer.forEach(item => {
                let newContentDiv = `<div class="content ${item['classFilter']}">
                                    <img src="img/${item['img']}" alt="${item['classFilter']}">
                                    <div class="title ${item['classFilter']}">
                                        <div class="buttons">
                                            <button><i class="fas fa-link"></i></button>
                                            <button><i class="fas fa-search"></i></button>
                                        </div>
                                        <div class="title">creative design</div>
                                        <div class="description">${item['description']}</div>
                                    </div>
                                 </div>`;

                contentDiv.after(newContentDiv);
            });
        },3000);

        if(btnClicked === 2){
            $(this).remove();
        }
    });

    const dataFromServer = [{
        img: 'work-layer2-web.png',
        classFilter: 'gr-des',
        description: 'Graphic Design'
    }, {
        img: 'work-layer4-web.png',
        classFilter: 'web-des',
        description: 'Web Design'
    }, {
        img: 'work-layer6-web.png',
        classFilter: 'web-des',
        description: 'Web Design'
    }, {
        img: 'work-layer10-web.png',
        classFilter: 'lan-p',
        description: 'Landing Page'
    }, {
        img: 'work-layer8-web.png',
        classFilter: 'lan-p',
        description: 'Landing Page'
    }, {
        img: 'work-layer7-web.png',
        classFilter: 'lan-p',
        description: 'Landing Page'
    }, {
        img: 'work-layer11-web.png',
        classFilter: 'gr-des',
        description: 'Graphic Design'
    }, {
        img: 'work-layer4-web.png',
        classFilter: 'web-des',
        description: 'Web Design'
    }, {
        img: 'work-layer3-web.png',
        classFilter: 'wp',
        description: 'Wordpress'
    }, {
        img: 'work-layer5-web.png',
        classFilter: 'gr-des',
        description: 'Graphic Design'
    }, {
        img: 'work-layer9-web.png',
        classFilter: 'web-des',
        description: 'Web Design'
    }, {
        img: 'work-layer1-web.png',
        classFilter: 'wp',
        description: 'Wordpress'
    }]

});
