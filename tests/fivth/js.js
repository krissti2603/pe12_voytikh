$(document).ready(function () {
    function isSpam(enteredUserString, spamWord, countSpamWord) {
        const re = new RegExp(spamWord, 'g');
        return (enteredUserString.match(re) || []).length >= countSpamWord;
    }

    $('#send-comment').click(function () {
        const comment = $('#comment').val();
        const spamWord = $('#spam-word').val();
        const countSpamWord = 3;
        const p = $('#spam-check-result');
        p.text(isSpam(comment, spamWord, countSpamWord));
    });
});
