let iconPasswordInputFirst = document.querySelector('.first-input .icon-password');
let iconPasswordInputSecond = document.querySelector('.second-input .icon-password');
let inputPasswordFirst = document.querySelector('.first-input input');
let inputPasswordSecond = document.querySelector('.second-input input');
let btn = document.querySelector('.btn');

iconPasswordInputFirst.addEventListener('click', (event) => {
    togglePassword(event.target);
});
iconPasswordInputSecond.addEventListener('click', (event) => {
    togglePassword(event.target);
});
btn.addEventListener('click', () => {
    if (inputPasswordFirst.value === inputPasswordSecond.value && inputPasswordFirst.value !== '') {
        alert('You are welcome!');
    } else {
        if (!document.querySelector('.error')) {
            let errorDiv = document.createElement('div');
            errorDiv.classList.add('error');
            errorDiv.innerHTML = 'Data in fields should be the same';
            inputPasswordSecond.after(errorDiv);
            errorDiv.style.color = 'red';
        }
    }
});

function togglePassword(iconElem) {
    const input = iconElem.parentElement.querySelector('input');
    if (input.type === "password") {
        input.type = "text";
        iconElem.classList.remove('fa-eye');
        iconElem.classList.add('fa-eye-slash')
    } else {
        input.type = "password";
        iconElem.classList.remove('fa-eye-slash');
        iconElem.classList.add('fa-eye')
    }
}
