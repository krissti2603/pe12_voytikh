
function userResult(firstN, secondN, mathOperation){
    let resultNum;
    if(mathOperation === '+'){
        resultNum = firstN + secondN;
    } else if(mathOperation === '-'){
        resultNum = firstN - secondN;
    } else if(mathOperation === '*'){
        resultNum = firstN * secondN;
    } else if(mathOperation === '/'){
        resultNum = firstN / secondN;
    }
    return resultNum;
}


const firstNumber = prompt('Enter first number: ');
const secondNumber = prompt('Enter second number: ');
const mathOperation = prompt('Enter math operation(+, -, *, /): ');
let firstN = Number(firstNumber);
let secondN = Number(secondNumber);

let resultR = userResult(firstNumber, secondNumber, mathOperation);

console.log('Result: ' + resultR);


