jQuery(document).ready(function ($) {

    let liElem =$('#slider ul li');
    let slideCount = liElem.length;
    let slideWidth = liElem.width();
    let sliderUlWidth = slideCount * slideWidth;
    let slideHeight = liElem.height();


    $('#slider').css({ width: slideWidth, height: slideHeight });

    $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
         $('#slider ul').animate({
            left: + slideWidth
        }, 400, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });

    }

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 400, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    }

    $('a.control_prev').click(function () {
        event.preventDefault();
        moveLeft();
    });

    $('a.control_next').click(function () {
        event.preventDefault();
        moveRight();
    });

    // Header menu for 480px
    $('.btn-toggle').click(function () {
        event.preventDefault();
        $('.header-menu-dropDown-links').toggleClass('open');

    });

});
