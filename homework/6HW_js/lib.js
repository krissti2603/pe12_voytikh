function filterBy(arr, type){
    return arr.filter(item => typeof item !== type);
}

let arr =  ['hello', 'world', 23, '23', null];
console.log(filterBy(arr, 'string'));