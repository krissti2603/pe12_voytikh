const labelInput = document.createElement('label');
const priceInput = document.createElement('input');
const errorDiv = document.createElement('div');
const infoSpan = document.createElement('span');
const btnCross = document.createElement('button');
priceInput.setAttribute('type', 'number');
labelInput.innerText = 'Price, $ ';
document.body.append(labelInput);
labelInput.append(priceInput);

priceInput.addEventListener('focus', function () {
    priceInput.removeAttribute('style');
    errorDiv.innerText = '';
    priceInput.style.outline = 'none';
    priceInput.style.border = '3px solid green';
});
priceInput.addEventListener('blur', function () {
    priceInput.removeAttribute('style');
    let price = priceInput.value;
    if (price <= 0) {
        if (infoSpan) {
            infoSpan.remove();
        }
        priceInput.style.border = '1px solid red';
        labelInput.append(errorDiv);
        errorDiv.innerText = 'Please enter correct price';
        errorDiv.style.color = 'red';
    } else {
        labelInput.before(infoSpan);
        infoSpan.innerText = `Текущая цена: ${price} $`;
        infoSpan.append(btnCross);
        btnCross.innerText = 'X';
        infoSpan.style.display = 'block';
        priceInput.style.color = 'green';
    }
});
btnCross.addEventListener('click', () => {
    priceInput.removeAttribute('style');
    btnCross.remove();
    infoSpan.remove();
    priceInput.value = '';
});
