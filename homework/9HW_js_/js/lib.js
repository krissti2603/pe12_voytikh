let tabsUl = document.querySelector('.tabs');
let tabsTitle = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelectorAll('.tabs-content li');

tabsUl.addEventListener('click', (event) => {
    let activeLi = event.target;
    let activeIndex;
    tabsTitle.forEach((item, index) => {
        item.classList.remove('active');
        if (activeLi === item) {
            activeIndex = index;
        };
    });
    activeLi.classList.add('active');
    tabsContent.forEach((itemContent) => {
        itemContent.classList.remove('tabs-content-text');
    });
    tabsContent.item(activeIndex).classList.add('tabs-content-text');
});



