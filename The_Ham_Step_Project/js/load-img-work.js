$(document).ready(function () {
    let btnClicked = 0;

    $('.wrapper-work > .btn .load-more').click(function () {
        const contentDiv = $('.wrapper-work .tabs-content > div:last-child');
        const animationDiv = $('.wrapper-work .container');

        btnClicked += 1;
        animationDiv.slideDown("slow").css('display', 'flex');

        setTimeout(function () {
            animationDiv.slideDown("slow").css('display', 'none');
            dataFromServer.forEach(item => {
                let newContentDiv = `<div class="content ${item['classFilter']}">
                                    <img src="img/${item['img']}" alt="${item['classFilter']}">
                                    <div class="title ${item['classFilter']}">
                                        <div class="buttons">
                                            <button><i class="fas fa-link"></i></button>
                                            <button><i class="fas fa-search"></i></button>
                                        </div>
                                        <div class="title">creative design</div>
                                        <div class="description">${item['description']}</div>
                                    </div>
                                 </div>`;

                contentDiv.after(newContentDiv);
            });
        },3000);

        if(btnClicked === 2){
            $(this).remove();
        }
    });

    const dataFromServer = [{
        img: 'work-layer2-web.png',
        classFilter: 'gr-des',
        description: 'Graphic Design'
    }, {
        img: 'work-layer4-web.png',
        classFilter: 'web-des',
        description: 'Web Design'
    }, {
        img: 'work-layer6-web.png',
        classFilter: 'web-des',
        description: 'Web Design'
    }, {
        img: 'work-layer10-web.png',
        classFilter: 'lan-p',
        description: 'Landing Page'
    }, {
        img: 'work-layer8-web.png',
        classFilter: 'lan-p',
        description: 'Landing Page'
    }, {
        img: 'work-layer7-web.png',
        classFilter: 'lan-p',
        description: 'Landing Page'
    }, {
        img: 'work-layer11-web.png',
        classFilter: 'gr-des',
        description: 'Graphic Design'
    }, {
        img: 'work-layer4-web.png',
        classFilter: 'web-des',
        description: 'Web Design'
    }, {
        img: 'work-layer3-web.png',
        classFilter: 'wp',
        description: 'Wordpress'
    }, {
        img: 'work-layer5-web.png',
        classFilter: 'gr-des',
        description: 'Graphic Design'
    }, {
        img: 'work-layer9-web.png',
        classFilter: 'web-des',
        description: 'Web Design'
    }, {
        img: 'work-layer1-web.png',
        classFilter: 'wp',
        description: 'Wordpress'
    }]
});